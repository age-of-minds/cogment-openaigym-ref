import cog_settings
from data_pb2 import AgentAction

from cogment import Agent, GrpcServer
import pickle


class Agent(Agent):
    VERSIONS = {"agent": "1.0.0"}
    actor_class = cog_settings.actor_classes.agent

    def __init__(self, trial):
        super().__init__(self)
        self.action_space = None
    
    def decide(self, observation):
        if not self.action_space:
            self.action_space = pickle.loads(observation.pickled_action_space)


        state = pickle.loads(observation.pickled_state)

        # Choose action based on state
        action = self.action_space.sample()

        return AgentAction(pickled_action=pickle.dumps())

    def reward(self, reward):
        pass

    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(Agent, cog_settings)
    server.serve()