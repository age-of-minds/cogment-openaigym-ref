import cogment as _cog
from types import SimpleNamespace

import data_pb2
import delta

_agent_class = _cog.ActorClass(
    name='agent',
    action_space=data_pb2.AgentAction,
    observation_space=data_pb2.Observation,
    observation_delta=data_pb2.ObservationDelta,
    observation_delta_apply_fn=delta.apply_delta,
    feedback_space=None
)

_human_class = _cog.ActorClass(
    name='human',
    action_space=data_pb2.HumanAction,
    observation_space=data_pb2.Observation,
    observation_delta=data_pb2.ObservationDelta,
    observation_delta_apply_fn=delta.apply_delta,
    feedback_space=None
)


actor_classes = SimpleNamespace(
    agent=_agent_class,
    human=_human_class,
)


environment = SimpleNamespace(
    config=data_pb2.Config,
    actors=[
        (_agent_class, 1),
        (_human_class, 1),
    ]
)
