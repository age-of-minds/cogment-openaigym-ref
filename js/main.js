import {Connection} from 'cogment';
import cog_settings from './cog_settings';

const pako = require('pako');
const protos = require("./data_pb")


function display(observation) {
  console.log(observation.toObject())

  const w = observation.getImageWidth();
  const h = observation.getImageHeight();

  var canvas = document.querySelector('#render');
  canvas.width = w;
  canvas.height = h;


  var ctx = canvas.getContext("2d");
  ctx.fillStyle = "blue";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  
  var img_data = pako.inflate(observation.getImage());
  console.log(img_data)

  var image_data = new Uint8ClampedArray(w * h * 4)


  for(var i = 0 ; i < w; ++i) {
    for(var j = 0 ; j < h; ++j) {
      const index = i * h + j;
      image_data[index * 4] = img_data[index*3];
      image_data[index * 4 + 1] = img_data[index*3 + 1];
      image_data[index * 4 + 2] = img_data[index*3 + 2];
      image_data[index * 4 + 3 ] = 255;
    }
  }

  var image = new ImageData(image_data, w, h);
  console.log(image.data)

  ctx.putImageData(image, 0, 0);
}

async function advance(trial) {
  var action = new protos.HumanAction();
  var obs = await trial.do_action(action);

  display(obs);
}

async function launch() {
  var step_btn = document.querySelector("#step");
  var run_btn = document.querySelector("#run");
  var start_btn = document.querySelector("#start");
  var end_btn = document.querySelector("#end");
  var name_input = document.querySelector("#env");
  
  var aom_conn = new Connection(cog_settings, "http://127.0.0.1:8088")

  var trial = undefined;
  var ready = false;
  var running = false;

  start_btn.onclick = async function() {
    var cfg = new protos.Config();
    cfg.setEnvName(name_input.value);
  
    trial = await aom_conn.start_trial(cog_settings.actor_classes.human, cfg);
    ready = true;
  
    display(trial.observation);
  }


  step_btn.onclick = async function() {
    if(!ready) return;

    ready = false
    
    advance(trial);
    ready = true;
  };

  run_btn.onclick = async function() {
    running = true;
    while(running) {
      await advance(trial);
    }
  };

  end_btn.onclick = async function() {
    running = false;
    trial.end()
    trial = undefined;
    ready = false;
  };
}

window.setTimeout(launch, 1000);


