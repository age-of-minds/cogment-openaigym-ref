import {apply_delta_replace} from 'cogment/delta_encoding'

import * as data_pb2 from './data_pb.js';
import * as delta from './delta.js';

const _agent_class = {
    name: 'agent',
    action_space: data_pb2.AgentAction,
    observation_space: data_pb2.Observation,
    observation_delta: data_pb2.ObservationDelta,
    observation_delta_apply_fn: delta.apply_delta,
    feedback_space: undefined
};

const _human_class = {
    name: 'human',
    action_space: data_pb2.HumanAction,
    observation_space: data_pb2.Observation,
    observation_delta: data_pb2.ObservationDelta,
    observation_delta_apply_fn: delta.apply_delta,
    feedback_space: undefined
};


const settings = {
  actor_classes: {
    agent: _agent_class,
    human: _human_class,
  },

  environment: {
    config: data_pb2.Config,
    actors: [
        {actor_class: _agent_class, count:1},
        {actor_class: _human_class, count:1},
    ]
  }
};

export default settings;