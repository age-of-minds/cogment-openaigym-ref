FROM python:3.7


RUN apt-get update && apt-get install -y xvfb python-opengl
RUN pip install cogment pygame box2d-py gym[atari]

WORKDIR /app
