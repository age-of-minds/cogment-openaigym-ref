import cog_settings
from data_pb2 import Observation, ObservationDelta

from cogment import Environment, GrpcServer
import gym
import video
import pickle
import traceback
from threading import Thread, Lock

mutex = Lock()

class Env(Environment):
    VERSIONS = {"env": "1.0.0"}

    def start(self, config):
        try:
            mutex.acquire()
            print(f"environment starting: {config.env_name}")
            self.stream = video.StreamWriter()

            self.gym_env = gym.make(config.env_name)

            state = self.gym_env.reset()
            image = self.gym_env.render(mode='rgb_array')

            observation = Observation(
                env_name=config.env_name,
                image_width=image.shape[1],
                image_height=image.shape[0],
                pickled_action_space=pickle.dumps(self.gym_env.action_space),
                pickled_state=pickle.dumps(state),
                image=self.stream.compress(image)
            )
            return observation
        except:
            traceback.print_exc()
            raise
        finally:
            mutex.release()
    def update(self, actions):
        try:
            mutex.acquire()
            action = pickle.loads(actions.agent[0].pickled_action)
            state, reward, done, info = self.gym_env.step(action)
            image = self.gym_env.render(mode='rgb_array')
            delta = ObservationDelta(
                image=self.stream.compress(image),
                pickled_state=pickle.dumps(state))
            return delta
        except:
            traceback.print_exc()
            raise
        finally:
            mutex.release()

    def end(self):
        print("ending...")
#       self.gym_env.close()


if __name__ == "__main__":
    server = GrpcServer(Env, cog_settings)
    server.serve()
