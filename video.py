import pickle
import gzip
import numpy as np

class StreamWriter:
    def __init__(self):
        pass

    def compress(self, rgb_array):
        rgb_array = rgb_array.astype(np.uint8)
        return gzip.compress(rgb_array.tobytes())


class StreamReader:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def decompress(self, bytes):
        rgb_array = np.frombuffer(gzip.decompress(bytes), dtype=np.uint8)
        rgb_array = np.reshape(rgb_array, (self.width, self.height, 3))
        rgb_array = np.transpose(rgb_array, (1,0,2))
        return rgb_array
