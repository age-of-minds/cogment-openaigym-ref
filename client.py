# client.py
import cog_settings

from data_pb2 import HumanAction, Config
from cogment.client import Connection

import numpy
import pygame
import video
import sys

# Create a connection to the Orchestrator serving this project
conn = Connection(cog_settings, "127.0.0.1:9000")
#conn = Connection(cog_settings, "orchestrator:9000")

# Initiate a trial
trial = conn.start_trial(cog_settings.actor_classes.human, Config(
  env_name=sys.argv[1]
))

window = pygame.display.set_mode(
          (trial.observation.image_height, trial.observation.image_width), 
          pygame.DOUBLEBUF
        )

stream = video.StreamReader(
  trial.observation.image_width,
  trial.observation.image_height
)

for i in range(10000):
    observation = trial.do_action(HumanAction())
    data = stream.decompress(observation.image)

    pygame.pixelcopy.array_to_surface(window, data)
    pygame.display.flip()

# cleanup
trial.end()
