This is a sample bootstraping project for the Cogment python SDK.

Clone this repository, and you should be good to go.

## Useful commands:

Compile the cogment.yaml and proto files:
```
docker-compose run cogment-cli --file /data/cogment.yaml --python_dir /data 
or
cogment-cli config generate --python_dir=.
```

Launch the environment, agent and trial servers:
```
docker-compose up orchestrator agent env
```

Run the client application:
```
docker-compose run client
```
